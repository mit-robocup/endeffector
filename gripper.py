#!/usr/bin/env python2
from __future__ import division, print_function

import zmq
import threading
import time
import simplejson


GRIPPER_PORT = 5556


CLOSE_GOAL = 160
OPEN_GOAL = 500
MAX_LOAD = 1000


class DxlWrapper:
    current_pos = None
    current_load = None
    goal_pos = None
    speed = None

    def __init__(self):
        # TODO: Init dynamixel

        self.stop_running = True
        self.runner = None

    def start(self):
        def run():
            while not self.stop_running:
                # TODO: Update, send command, monitor load, etc.
                time.sleep(0.001)

        self.stop_running = False
        self.runner = threading.Thread(target=run)
        self.runner.start()

    def stop(self):
        self.stop_running = True
        if self.runner is not None:
            self.runner.join()


if __name__ == "__main__":
    dxl_wrapper = DxlWrapper()
    dxl_wrapper.start()

    ctx = zmq.Context()
    sock = ctx.socket(zmq.REP)
    sock.bind("tcp://*:{}".format(GRIPPER_PORT))

    try:
        while True:
            try:
                status = {
                    "position": dxl_wrapper.current_pos,
                    "load": dxl_wrapper.current_load
                }

                req = sock.recv_json()

                goal_pos = None
                speed = None

                if "direction" in req:
                    if req["direction"] == "open":
                        # Open gripper
                        goal_pos = CLOSE_GOAL
                    elif req["direction"] == "close":
                        # Close gripper
                        goal_pos = OPEN_GOAL
                    else:
                        raise RuntimeError("Invalid direction")

                    if "speed" in req:
                        # Set speed
                        speed = float(req["speed"])
                    else:
                        raise RuntimeError("Must specify speed")

                dxl_wrapper.goal_pos = goal_pos
                dxl_wrapper.speed = speed

                sock.send_json(status)
            except simplejson.scanner.JSONDecodeError as e:
                print("invalid request")
                sock.send(b"err")
            except RuntimeError as e:
                print("invalid request")
                sock.send(b"err")
            except ValueError as e:
                print("invalid request")
                sock.send(b"err")
    except KeyboardInterrupt:
        dxl_wrapper.stop()
        print()
