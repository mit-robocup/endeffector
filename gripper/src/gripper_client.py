#!/usr/bin/env python2

import zmq
import simplejson
import time


GRIPPER_PORT = 5556
GRIPPER_IP = "127.0.0.1"


if __name__ == "__main__":
    ctx = zmq.Context()
    sock = ctx.socket(zmq.REQ)
    sock.connect("tcp://{}:{}".format(GRIPPER_IP, GRIPPER_PORT))

    while True:
        try:
            sock.send_json({
                "direction": "open",
                "speed": 0 # Max speed
            })
            res = sock.recv_json()
            print(res)
        except simplejson.scanner.JSONDecodeError:
            print("invalid response")

        time.sleep(1)
